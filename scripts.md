# export
- mongoexport --uri=mongodb://127.0.0.1:27017 --db=waktusolat-api --collection=zones --out=zones.json
- mongoexport --uri=mongodb://127.0.0.1:27017 --db=waktusolat-api --collection=pray_times --out=pray_times.json

# import
- mongoimport --db=waktusolat-api --collection=zones --file=zones.json 
- mongoimport --db=waktusolat-api --collection=pray_times --file=pray_times.json 